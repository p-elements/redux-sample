'use strict';

var webpack = require('webpack');
var fs = require('fs');
var path = require('path');

const babelOptions = {
  "presets": [
    ["@babel/preset-env", {
      "targets": {
        "browsers": require("./package.json").browserslist
      }
    }],
    ["@babel/preset-react", {
      "pragma": "h"
    }]
  ],
  "plugins": [
    ["@babel/plugin-proposal-class-properties", { "loose": true }],
    "@babel/proposal-object-rest-spread",
    ['jsx-auto-key-attr']
  ]
};

module.exports = function (env) {
  const plugins = [new webpack.NamedModulesPlugin()];
  let mode = 'development';
  let rules = [
    {
      test: /\.ts(x?)$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'babel-loader',
          options: babelOptions
        },
        {
          loader: 'ts-loader'
        }
      ]
    }, {
      test: /\.css$/,
      exclude: /node_modules/,
      use: [{
          loader: 'typings-for-css-modules-loader',
          options: {
            namedExport: true,
            modules: false,
            silent: true,
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            plugins: function () {
              return [
                require('autoprefixer')({
                  grid: true,
                  browsers: require("./package.json").browserslist
                }),
                require('postcss-clean')
              ];
            },
          },
        },
      ]
    }
  ];

  if (process.env.NODE_ENV === 'test') {
    rules.push({
      test: /\.ts(x?)$/,
      exclude: /(node_modules|\.spec\.ts$)/,
      loader: 'istanbul-instrumenter-loader',
      enforce: 'post',
      options: {
        esModules: true
      }
    });
  }

  if (process.env.NODE_ENV !== 'test' && process.env.NODE_ENV !== 'dev') {
    mode = 'production';
  }

  return {
    mode,
    devServer: {
      contentBase: path.join(__dirname ),
      compress: true,
      useLocalIp: true,
      disableHostCheck: true,
      host: '0.0.0.0',
      port: 9000,
      open: true,
      openPage: 'demo/index.html',
      publicPath: '/dist/',
      // sample app before
      // before:(app) => {
      //   app.get('/api/foo', function(req, res) {
      //     res.sendFile(path.join(__dirname, '/demo/api/foo.json'));
      //   });
      // }
    },
    cache: true,
    externals: {
      'p-elements-core': 'CustomElement',
      'maquette': 'Maquette',
      'underscore': '_',
      'animejs': 'anime'
    },
    context: path.resolve(__dirname, './src'),
    entry: {
      'sample-app': [
        './app.tsx'
      ],
    },
    devtool: (mode !== 'production') ? 'inline-source-map' : false,
    output: {
      path: path.resolve(__dirname, './dist'),
      filename: '[name]' + '.js',
      chunkFilename: '[chunkhash]' + '.js'
    },
    module: {
      rules: rules
    },
    plugins: plugins,
    resolve: {
      extensions: ['.ts', '.tsx', '.js']
    },
  }

};
