import { Action, ActionCreator } from "redux";

export const GET_USERS_REQUEST = "GET_USERS_REQUEST";
export const GET_USERS_REQUEST_ERROR = "GET_USERS_REQUEST_ERROR";
export const GET_USERS_REQUEST_SUCCESS = "GET_USERS_REQUEST_SUCCESS";

export interface UsersActionRequest extends Action<"GET_USERS_REQUEST"> {}
export interface UsersActionRequestError extends Action<"GET_USERS_REQUEST_ERROR"> {}
export interface UsersActionRequestSuccess extends Action<"GET_USERS_REQUEST_SUCCESS"> {
  data: User[];
}

export type UsersAction = UsersActionRequest | UsersActionRequestError | UsersActionRequestSuccess;

export const requestUsers: ActionCreator<UsersActionRequest> = () => {
  return {
    type: GET_USERS_REQUEST,
  };
};

export const requestUsersError: ActionCreator<UsersActionRequestError> = () => {
  return {
    type: GET_USERS_REQUEST_ERROR,
  };
};

export const requestUsersSuccess: ActionCreator<UsersActionRequestSuccess> = (data: User[]) => {
  return {
    data,
    type: GET_USERS_REQUEST_SUCCESS,
  };
};

export interface Geo {
  lat: number;
  lng: number;
}

export interface Address {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: Geo;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  phone: string;
  website: string;
  company: Company;
}

export const getUsers: ActionCreator<any> = () => {
  return (dispatch) => {
    dispatch(requestUsers());
    const url = `https://jsonplaceholder.typicode.com/users/`;
    return fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data: User[]) => {
        dispatch(requestUsersSuccess(data));
      })
      .catch(() => {
        dispatch(requestUsersError());
      });
  };
};
