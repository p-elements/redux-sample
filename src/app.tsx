import "p-elements-core";

import { Store } from "redux";
import { getUsers } from "./actions/users";
import { CounterComponent } from "./counterComponent";
import { RootState, store } from "./store";

import * as css from "./app.css";

export interface StateApp {
  store: Store;
  state: RootState;
}

@CustomElementConfig({
  tagName: "sample-app",
})
export class SampleAppElement extends CustomElement implements StateApp {

  static get observedAttributes(): string[] {
    return ["name"];
  }

  @PropertyRenderOnSet
  public name: string = "";

  public state: RootState;

  public store: Store<any> = store;

  private storeUnsubscribe;

  private counter: CounterComponent | undefined = new CounterComponent(this);

  private stateChanged = (newState: RootState) => {
    this.state = newState;
    this.renderNow();
  }

  private render = () => {

    return <div id="Sample" classes={
      {
        "sample": true,
        "sample__name-is-peter": this.name.toLowerCase() === "peter",
      } }>

      {this.counter.render()}

      <p>Hello {this.name.length > 0 ? this.name : "World"}!!</p>
      <p><label for="Name">Naam</label> <input id="Name" oninput={this.onInput} value={this.name} /></p>
      <ul>{this.state && this.state.users && this.state.users.data  ?
        this.state.users.data.map((user) => {
          return <li><strong>{user.name}</strong><br />
            <span>{user.address.street}</span><br />
            <span>{user.address.zipcode} {user.address.city}</span><br />
          </li>;
        })
        : null}</ul>
    </div>;
  }

  private onInput =  (event: Event) => {
    this.name = (event.target as HTMLInputElement).value;
    this.dispatchEvent(new CustomEvent("nameChange", {
      bubbles: false,
      cancelable: true,
      detail: this.name,
    }));
  }

  private connectedCallback() {
    const template = this.templateFromString(`
      <style>${css}</style>
      <div class="root"></div>`, true);
    this.shadowRoot.appendChild(template);

    const rootElement = this.shadowRoot.querySelector(".root");
    this.createProjector(rootElement, this.render);
    this.storeUnsubscribe = this.store.subscribe(() => this.stateChanged(this.store.getState()));
    this.store.dispatch(getUsers());
  }

  private disconnectedCallback() {
    if (this.storeUnsubscribe) {
      this.storeUnsubscribe();
    }
  }

  private attributeChangedCallback(name: string, oldValue: string, newValue: string) {
    switch (name) {
      case "name":
        this.name = newValue;
        break;
    }
  }

}
