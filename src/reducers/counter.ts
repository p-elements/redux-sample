import { Reducer } from "redux";
import { DECREMENT,  INCREMENT } from "../actions/counter";
import { RootAction } from "../store";

export interface CounterState {
  clicks: number;
  teller: number;
}

const defaultState = {
  clicks: 0,
  teller: 0,
};

export const counter: Reducer<CounterState, RootAction> = ( state = defaultState, action) => {
  switch (action.type) {

    case DECREMENT:
      return {
        clicks: state.clicks + 1,
        teller: state.teller - 1,
      };

    case INCREMENT:
      return {
        clicks: state.clicks + 1,
        teller: state.teller + 1,
      };

    default:
      return {...state};

  }
};
