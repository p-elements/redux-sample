import { Reducer } from "redux";
import { GET_USERS_REQUEST, GET_USERS_REQUEST_ERROR, GET_USERS_REQUEST_SUCCESS, User } from "../actions/users";
import { RootAction } from "../store";

export interface UsersState {
  data: User[];
}

const defaultState = {
  data: [],
};

export const users: Reducer<UsersState, RootAction> = ( state = defaultState, action) => {
  switch (action.type) {

    case GET_USERS_REQUEST:
      return state;

    case GET_USERS_REQUEST_ERROR:
      return state;

    case GET_USERS_REQUEST_SUCCESS:
      return {
        ...state,
        data: action.data,
      };

    default:
      return state;

  }
};
