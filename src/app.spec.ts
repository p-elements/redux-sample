import * as fetchMock from "fetch-mock";

import { SampleAppElement } from "./app";

/**
 * Function to wait for elements to be rendered
 *
 * @param {(Element | ShadowRoot)} element
 * @param {string} selector
 * @param {((node: Element | null) => void)} callback
 * @param {number} [timeout]
 */
function waitForSelector(
  element: Element | ShadowRoot,
  selector: string,
  callback: (node: Element | null) => void,
  timeout?: number) {
  if (!timeout) { timeout = 2000; }

  if (element.querySelector(selector)) {
    callback(element.querySelector(selector));
  } else {
    let observer: MutationObserver;
    let timer: any;

    observer = new MutationObserver((mutations) => {
      for (const mutation of mutations) {
        for (const addedNode of [].slice.call(mutation.addedNodes)) {
          if (addedNode.matches && addedNode.matches(selector)) {
            if (timer) { clearTimeout(timer); }
            setTimeout(() => {
              callback(addedNode);
            }, 10);
            observer.disconnect();
            return;
          }
        }
      }
    });

    timer = setTimeout(() => {
      if (element.querySelector(selector)) {
        callback(element.querySelector(selector));
      } else {
        callback(null);
      }
      observer.disconnect();
    }, timeout);

    observer.observe(element, {
      childList: true,
      subtree: true,
    });
  }
}

/**
 * Fetch mock for the theme.css call
 */
fetchMock.get("theme.css",
  `:root{
    --p-sample-color: rgb(255, 100, 0);
  }`);

describe("sample-app element", () => {

  /**
   * Creates new component before every test
   */
  beforeEach(() => {
    document.body.appendChild(document.createElement("sample-app"));
  });

  /**
   * Deletes component after every test and reset fetchmock
   */
  afterEach(() => {
    const element = document.querySelector("sample-app");
    if (element) {
      element.parentNode.removeChild(element);
    }
    fetchMock.reset();
  });

  it("Should be defined", (done) => {
    customElements.whenDefined("sample-app").then(() => {
      done();
    });
  });

  it("Should have shadow root", (done) => {
    const element = document.querySelector("sample-app") as SampleAppElement;
    expect(typeof element.shadowRoot).toBe("object");
    done();
  });

  it("Should have correct styling from css variables", (done) => {

    if (!document.documentElement.querySelector(`[is="custom-style"]`)) {
      const customStyle = (document as any).createElement("link", {is: "custom-style"});
      customStyle.setAttribute("href", "theme.css");
      document.documentElement.querySelector("head").appendChild(customStyle);
    }

    const element = document.querySelector("sample-app") as SampleAppElement;

    waitForSelector(element.shadowRoot, ".sample", (sampleElement: HTMLElement) => {
      const elementStyle = window.getComputedStyle(sampleElement);
      expect(elementStyle.backgroundColor).toBe("rgb(255, 100, 0)");
      done();
    });

  });

});
