import { decrement, increment } from "./actions/counter";
import { StateApp } from "./app";

export class CounterComponent {

  constructor(private app: StateApp) {}

  public render = () => {
    return <div>
        <a href="#" onclick={this.onMinClickHandler}>Min</a> <a href="#" onclick={this.onPlusClickHandler}>Plus</a>
        <p>
          Teller: {this.app.state && this.app.state.counter ? this.app.state.counter.teller : "0"} <br />
          Clicks:  {this.app.state && this.app.state.counter ? this.app.state.counter.clicks : "0"}
        </p>
      </div>;
  }

  private onPlusClickHandler = (e: MouseEvent) => {
    e.preventDefault();
    this.app.store.dispatch(increment());
  }

  private onMinClickHandler = (e: MouseEvent) => {
    e.preventDefault();
    this.app.store.dispatch(decrement());
  }

}
