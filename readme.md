# SampleApp

`<sample-app>` element

Sample app

## Install npm packages

```
npm install
```

## Build
```
npm run build
```

## Develop

```
npm run develop
```

## Test
```
npm test
```

## Before submitting changes
lint
```
npm run lint
```
