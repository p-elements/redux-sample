import { StateApp } from "./app";
export declare class CounterComponent {
    private app;
    constructor(app: StateApp);
    render: () => any;
    private onPlusClickHandler;
    private onMinClickHandler;
}
