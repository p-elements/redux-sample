export declare const lazyReducerEnhancer: (combineReducers: any) => (nextCreator: any) => (origReducer: any, preloadedState: any) => any;
