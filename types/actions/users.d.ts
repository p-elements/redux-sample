import { Action, ActionCreator } from "redux";
export declare const GET_USERS_REQUEST = "GET_USERS_REQUEST";
export declare const GET_USERS_REQUEST_ERROR = "GET_USERS_REQUEST_ERROR";
export declare const GET_USERS_REQUEST_SUCCESS = "GET_USERS_REQUEST_SUCCESS";
export interface UsersActionRequest extends Action<"GET_USERS_REQUEST"> {
}
export interface UsersActionRequestError extends Action<"GET_USERS_REQUEST_ERROR"> {
}
export interface UsersActionRequestSuccess extends Action<"GET_USERS_REQUEST_SUCCESS"> {
    data: User[];
}
export declare type UsersAction = UsersActionRequest | UsersActionRequestError | UsersActionRequestSuccess;
export declare const requestUsers: ActionCreator<UsersActionRequest>;
export declare const requestUsersError: ActionCreator<UsersActionRequestError>;
export declare const requestUsersSuccess: ActionCreator<UsersActionRequestSuccess>;
export interface Geo {
    lat: number;
    lng: number;
}
export interface Address {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: Geo;
}
export interface Company {
    name: string;
    catchPhrase: string;
    bs: string;
}
export interface User {
    id: number;
    name: string;
    username: string;
    email: string;
    address: Address;
    phone: string;
    website: string;
    company: Company;
}
export declare const getUsers: ActionCreator<any>;
