import { Action, ActionCreator } from "redux";
export declare const DECREMENT = "DECREMENT";
export declare const INCREMENT = "INCREMENT";
export interface CounterActionIncrement extends Action<"INCREMENT"> {
}
export interface CounterActionDecrement extends Action<"DECREMENT"> {
}
export declare type CounterAction = CounterActionIncrement | CounterActionDecrement;
export declare const increment: ActionCreator<CounterActionIncrement>;
export declare const decrement: ActionCreator<CounterActionDecrement>;
