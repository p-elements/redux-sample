import "p-elements-core";
import { Store } from "redux";
import { RootState } from "./store";
export interface StateApp {
    store: Store;
    state: RootState;
}
export declare class SampleAppElement extends CustomElement implements StateApp {
    static readonly observedAttributes: string[];
    name: string;
    state: RootState;
    store: Store<any>;
    private storeUnsubscribe;
    private counter;
    private stateChanged;
    private render;
    private onInput;
    private connectedCallback;
    private disconnectedCallback;
    private attributeChangedCallback;
}
