import { Reducer } from "redux";
import { RootAction } from "../store";
export interface CounterState {
    clicks: number;
    teller: number;
}
export declare const counter: Reducer<CounterState, RootAction>;
