import { Reducer } from "redux";
import { User } from "../actions/users";
import { RootAction } from "../store";
export interface UsersState {
    data: User[];
}
export declare const users: Reducer<UsersState, RootAction>;
