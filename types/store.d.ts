import { CounterAction } from "./actions/counter";
import { UsersAction } from "./actions/users";
import { CounterState } from "./reducers/counter";
import { UsersState } from "./reducers/users";
export declare type RootAction = CounterAction | UsersAction;
export declare const store: any;
export interface RootState {
    counter?: CounterState;
    users?: UsersState;
}
